from django.contrib import admin
from .models import Auto, Titular
# Register your models here.
admin.site.register(Auto)
admin.site.register(Titular)