from django.db import models

class Auto(models.Model):
    color = models.CharField(max_length=30)
    cantidad_puertas = models.IntegerField()
    marca = models.CharField(max_length=40)
    modelo = models.CharField(max_length=40)
    anio_modelo = models.IntegerField()
    precio_compra = models.FloatField()
    precio_venta = models.FloatField()
    cant_titulares_anteriores = models.IntegerField()
    estado = models.CharField(
        max_length=10,
        choices=[
            ('vendido', 'Vendido'),('en venta', 'En venta')
        ],
        default='en venta',
    )
    modalidad_ingreso = models.CharField(
        max_length=15,
        choices=[
            ('compra', 'Compra'),('consignacion', 'Consignacion')
        ],
        default='compra',
    )
    def __str__(self) -> str:
        fields = {
            'marca': self.marca,
            'modelo': self.modelo,
            'anio_modelo': self.anio_modelo,
            'precio_compra': self.precio_compra,
            'precio_venta': self.precio_venta,
            'estado': self.estado
        }
        return str(fields)

class Titular(models.Model):
    apellido= models.CharField(max_length=30)
    nombre= models.CharField(max_length=30)
    telefono= models.CharField(max_length=30)
    direccion= models.CharField(max_length=30)
    email= models.CharField(max_length=30)

    def __str__(self) -> str:
        fields = {
            'apellido': self.apellido,
            'nombre': self.nombre,
            'telefono': self.telefono,
            'direccion': self.direccion,
            'email': self.email
        }
        return str(fields)